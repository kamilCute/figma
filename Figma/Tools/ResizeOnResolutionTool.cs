﻿using System;
using System.Windows;

namespace Figma.Tools
{
    public static class ResizeOnResolutionTool
    {
        public static int NewSize(object curSize)
        {
            /*From my observation:
             *Real Monitor's Width and Microsoft's PrimaryScreenWidth
             *1920 - 1738, 1680 - 1521, 1600 - 1449, 1400 - 1267, 1280 - 1159 */
            //Depending on what resolution (i.e. Width) user have - corresponding coefficient will multiply on.
            double curScreenWidth = (int)SystemParameters.PrimaryScreenWidth;
            double result = (int)curSize;
            if (curScreenWidth > 1521 && curScreenWidth <= 1740)
                result *= 0.9;
            else if (curScreenWidth > 1449 && curScreenWidth <= 1521)
                result *= 0.88;
            else if (curScreenWidth > 1267 && curScreenWidth <= 1449)
                result *= 0.62;
            else if (curScreenWidth > 1159 && curScreenWidth <= 1267)
                result *= 0.76;
            else if (curScreenWidth <= 1159)
                result *= 0.55;
            else
                result *= 1.1;
            return (int)result;
        }
    }
}
