﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Figma.Models
{
    class BalanceHistoryColumnsModel : INotifyPropertyChanged
    {
        #region Private members

        string transactionDate;

        string description;

        string balance;

        #endregion

        #region Properties

        public string TransactionDate
        {
            get => transactionDate;
            set
            {
                transactionDate = value;
                OnPropertyChanged();
            }
        }
        public string Description
        {
            get => description;
            set
            {
                description = value;
                OnPropertyChanged();
            }
        }
        public string Balance
        {
            get => balance;
            set
            {
                balance = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
