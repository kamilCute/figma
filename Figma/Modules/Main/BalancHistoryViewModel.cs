﻿using System;
using System.Collections.ObjectModel;
using Figma.Models;
using WpfCommon;

namespace Figma.Modules.Main
{
    class BalancHistoryViewModel : ViewModelBase
    {
        ObservableCollection<BalanceHistoryColumnsModel> elementsList;

        public ObservableCollection<BalanceHistoryColumnsModel> ElementsList
        {
            get => elementsList;
            set
            {
                elementsList = value;
                OnPropertyChanged();
            }
        }

        public BalancHistoryViewModel()
        {
            ObservableCollection<BalanceHistoryColumnsModel> elements = new ObservableCollection<BalanceHistoryColumnsModel>();
            elements.Add(new BalanceHistoryColumnsModel { Balance = "100.000$", Description = "Some Description_1", TransactionDate = "05.05.2020" });
            elements.Add(new BalanceHistoryColumnsModel { Balance = "8.000$", Description = "Some Description_2", TransactionDate = "01.01.2021" });
            elements.Add(new BalanceHistoryColumnsModel { Balance = "900$", Description = "Some Description_3", TransactionDate = "04.04.2020" });
            elements.Add(new BalanceHistoryColumnsModel { Balance = "5.000$", Description = "Some Description_4", TransactionDate = "11.03.2019" });
            ElementsList = elements;
        }
    }
}
