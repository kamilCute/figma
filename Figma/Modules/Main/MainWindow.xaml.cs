﻿using Figma.Modules.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Figma.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private members

        SettingsPage settingsPage;

        #endregion

        #region Properties

        public SettingsPage SettingsPage
        {
            get
            {
                if (settingsPage == null)
                    settingsPage = new SettingsPage();
                return settingsPage;
            }
        }

        #endregion

        #region ctor

        public MainWindow()
        {
            InitializeComponent();

            UpdateFrame(SettingsPage);
        }

        #endregion

        #region Event handlers

        private void mainLogo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            UpdateFrame(SettingsPage);
        }

        #endregion

        #region Private methods

        private void UpdateFrame(Page page)
        {
            frame.Content = page;
        }

        #endregion
    }
}
