﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Figma.Modules.Start
{
    /// <summary>
    /// Interaction logic for RegularClientPage.xaml
    /// </summary>
    public partial class RegularClientPage : Page
    {
        public RegularClientPage()
        {
            InitializeComponent();
        }

        private void txtPassBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pb = (PasswordBox)e.Source;
            if (pb.Password == "")
                txtPassBlock.Visibility = Visibility.Visible;
            else
                txtPassBlock.Visibility = Visibility.Hidden;
        }
    }
}
