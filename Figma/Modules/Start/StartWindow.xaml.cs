﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Figma.Modules.Start
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        RegisterPage registerPage;
        AuthPage authPage;
        public StartWindow()
        {
            InitializeComponent();

            authPage = new AuthPage();
            UpdateFrame(authPage);
        }

        private void btnTabAuth_Click(object sender, RoutedEventArgs e)
        {
            if (btnTabRegister.IsChecked == true)
            {
                btnTabRegister.IsChecked = false;
                btnTabAuth.IsChecked = true;
            }
            else if (!btnTabAuth.IsChecked == true)
            {
                btnTabAuth.IsChecked = true;
                return;
            }

            if (authPage == null)
                authPage = new AuthPage();
            UpdateFrame(authPage);
        }
        private void btnTabRegister_Click(object sender, RoutedEventArgs e)
        {
            if (btnTabAuth.IsChecked == true)
            {
                btnTabAuth.IsChecked = false;
                btnTabRegister.IsChecked = true;
            }
            else if (!btnTabRegister.IsChecked == true)
            {
                btnTabRegister.IsChecked = true;
                return;
            }

            if (registerPage == null)
                registerPage = new RegisterPage();
            UpdateFrame(registerPage);
        }
        private void UpdateFrame(Page page)
        {
            frameAuthRegister.Content = page;
        }

        private void changeThemeBtn_Click(object sender, RoutedEventArgs e)
        {
            var app = (App)Application.Current;
            app.ChangeTheme(uriFrom: new Uri("/Themes/MainTheme.xaml", UriKind.RelativeOrAbsolute),
                uriTo: new Uri("/Themes/MainTheme2.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
