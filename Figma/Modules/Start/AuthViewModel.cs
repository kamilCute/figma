﻿using System;
using WpfCommon;

namespace Figma.Modules.Start
{
    public class AuthViewModel : ViewModelBase
    {
        #region Properties
        public int TabBtnSize
        {
            get => Tools.ResizeOnResolutionTool.NewSize(67);
        }

        #endregion

        #region ctor

        public AuthViewModel()
        {
        }

        #endregion
    }
}