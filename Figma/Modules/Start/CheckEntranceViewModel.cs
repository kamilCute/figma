﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfCommon;
using Figma.Tools;

namespace Figma.Modules.Start
{
    class CheckEntranceViewModel : ViewModelBase
    {
        #region Private members

        string checkNumber;

        #endregion

        #region ctor

        public CheckEntranceViewModel()
        {

        }

        #endregion

        #region Properties

        public string CheckNumber
        {
            get => checkNumber;
            set
            {
                checkNumber = value;
                OnPropertyChanged();
            }
        }

        public int CheckBoxesSize
        {
            get => ResizeOnResolutionTool.NewSize(70);
            set { }
        }

        public double CheckFontSize
        {
            get => ResizeOnResolutionTool.NewSize(22);
        }

        public double BtnAuthFontSize
        {
            get => ResizeOnResolutionTool.NewSize(25);
        }
        public int BtnAuthSize
        {
            get => ResizeOnResolutionTool.NewSize(65);
        }

        #endregion
    }
}