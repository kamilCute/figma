﻿using System;
using Figma.Tools;
using WpfCommon;

namespace Figma.Modules.Start
{
    class StartViewModel : ViewModelBase
    {
        #region Properties

        public int TopMarginLogo
        {
            get => ResizeOnResolutionTool.NewSize(160);
        }

        public int LogoSize
        {
            get => ResizeOnResolutionTool.NewSize(146);
        }

        public int CentralPanelSize
        {
            get => ResizeOnResolutionTool.NewSize(570);
        }

        public double AuthRegisterFontSize
        {
            get => ResizeOnResolutionTool.NewSize(24);
        }

        public int AuthRegBtnSize
        {
            get => ResizeOnResolutionTool.NewSize(60);
        }

        #endregion

        #region ctor

        public StartViewModel()
        {
        }

        #endregion
    }
}
