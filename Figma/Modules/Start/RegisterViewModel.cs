﻿using System;
using WpfCommon;
using System.Windows.Input;

namespace Figma.Modules.Start
{
    class RegisterViewModel : ViewModelBase
    {
        #region Private members

        string login;

        string password;

        #endregion

        #region Properties

        public string Login
        {
            get => login;
            set
            {
                login = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => password;
            set
            {
                password = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        public ICommand AuthButtonCommand { get; set; }

        #endregion
    }
}