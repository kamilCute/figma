﻿using Figma.Tools;
using WpfCommon;

namespace Figma.Modules.Start
{
    class RegularClientViewModel : ViewModelBase
    {
        #region Private members

        string login;

        string password;

        #endregion

        #region Properties

        public string Login
        {
            get => login;
            set
            {
                login = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => password;
            set
            {
                password = value;
                OnPropertyChanged();
            }
        }

        public int BtnAuthSize
        {
            get => ResizeOnResolutionTool.NewSize(65);
        }

        public double BtnAuthFontSize
        {
            get => ResizeOnResolutionTool.NewSize(25);
        }

        public int LoginPassowrdBoxesSize
        {
            get => ResizeOnResolutionTool.NewSize(70);
        }
        public double BoxesFontSize
        {
            get => ResizeOnResolutionTool.NewSize(22);
        }

        #endregion

        #region ctor

        public RegularClientViewModel()
        {
        }

        #endregion

        #region Commands

        RelayCommand authButtonCommand;

        public RelayCommand AuthButtonCommand
        {
            get
            {
                return authButtonCommand ??
                    (authButtonCommand = new RelayCommand(obj =>
                    {
                        Views.MainWindow mainWindow = new Views.MainWindow();
                        mainWindow.ShowDialog();
                    }));
            }
        }

        #endregion
    }
}
