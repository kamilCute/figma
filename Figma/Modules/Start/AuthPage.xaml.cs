﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Figma.Modules.Start
{
    /// <summary>
    /// Interaction logic for AuthPage.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        RegularClientPage regularClientPage;
        CheckEntrancePage checkEntrancePage;
        public AuthPage()
        {
            InitializeComponent();

            regularClientPage = new RegularClientPage();
            UpdateFrame(regularClientPage);
        }
        private void btnTabRegularClient_Click(object sender, RoutedEventArgs e)
        {
            if (btntabCheckEntrance.IsChecked == true)
            {
                btntabCheckEntrance.IsChecked = false;
                btnTabRegularClient.IsChecked = true;
            }
            else if (!btnTabRegularClient.IsChecked == true)
                btnTabRegularClient.IsChecked = true;

            if (regularClientPage == null)
                regularClientPage = new RegularClientPage();
            UpdateFrame(regularClientPage);
        }
        private void btntabCheckEntrance_Click(object sender, RoutedEventArgs e)
        {
            if (btnTabRegularClient.IsChecked == true)
            {
                btnTabRegularClient.IsChecked = false;
                btntabCheckEntrance.IsChecked = true;
            }
            else if (!btntabCheckEntrance.IsChecked == true)
                btntabCheckEntrance.IsChecked = true;

            if (checkEntrancePage == null)
                checkEntrancePage = new CheckEntrancePage();
            UpdateFrame(checkEntrancePage);
        }
        private void UpdateFrame(Page page)
        {
            frameAuth.Content = page;
        }
    }
}