﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Figma
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            double curScreenWidth = (int)SystemParameters.PrimaryScreenWidth;
            double curScreenHeight = (int)SystemParameters.PrimaryScreenHeight;
        }
        public void ChangeTheme(Uri uriFrom, Uri uriTo)
        {
            Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = uriFrom });
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = uriTo });
        }
    }
}
